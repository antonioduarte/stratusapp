/**
 *
 * Network Server
 *
 * Copyright (c) 2014 Skyler Saleh
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
#ifndef SJS_VIDEO_TRANSMIT_H
#define SJS_VIDEO_TRANSMIT_H
#include "sjs_networking.h"
#include <opencv2/opencv.hpp>

#define REMAP_DEPTH(A) A
int sjsvt_compress_depth(unsigned short * in_data, unsigned short* out_data, int in_size){
    int current_count=0;
    int out_size =-1;
    int conv_current=-1;
    for(int i=0;i<in_size;++i){
        
        int d =in_data[REMAP_DEPTH(i)];
        int conv = 0;
        if(d>5000*0.2)conv=round(logf(d)*480.f-logf(5000.*0.2)*480.f);
        if(conv>2047)conv=0;
        
        if(conv_current!=conv || current_count==31){
            if(out_size>-1){
                out_data[out_size]= conv_current|(current_count<<11);
            }
            ++out_size;
            conv_current = conv;
            current_count=0;
        }
        ++current_count;
        
    }
    out_data[out_size++]= conv_current|(current_count<<11);
    
    return out_size;
}
int sjsvt_decompress_depth(unsigned short * in_data, unsigned short* out_data, int in_size, int max_out){
    int current_count=0;
    int current_conv=0;
    int current_out = 0;
    int current_in =0;
    while(current_out<max_out){
        if(current_count==0){
            
            int read = in_data[current_in];
            int data = read&0x7ff;
            current_count=read>>11;
            float d = data;
            current_conv = 0;
            if(d)
            current_conv=expf((d+logf(5000*0.2)*480.f)/480.);
            ++current_in;
        }
        out_data[REMAP_DEPTH(current_out)]=current_conv;
        current_out++;
        current_count--;
    }
    
    return current_in;
}
int sjsvt_compress_color(unsigned char* in_data, unsigned char* out_data, int max_out, int w, int h){
    cv::Mat col(h,w,CV_8UC3,in_data,0);
    std::vector<unsigned char> data;
    std::vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
    compression_params.push_back(80);
    cv::imencode(".jpg",col,data,compression_params);
    for(int i=0;i<data.size();++i){
        out_data[i]=data[i];
    }
    return data.size();
    
    
}
int sjsvt_decompress_color(unsigned char* in_data, unsigned char* out_data, int in_size, int w, int h){
    cv::Mat dat(in_size,1,CV_8UC1,in_data,0);
    cv::Mat col = cv::imdecode(dat,CV_LOAD_IMAGE_COLOR);
    unsigned char* data = col.data;
    int num_pix = col.cols*col.rows;
    for(int i=0;i<num_pix*3;++i){
        out_data[i]=data[i];
    }
    if(num_pix==0){
        std::cout<<"Failed to decompress color"<<std::endl;
    }
    return num_pix*3;
    
    
}
struct sjsvt_Frame{
    unsigned char *color;
    unsigned short *depth;
    //Measured Quantities
    double extra_data[16];
    // Pos[x,y,z] 0-2
    // rot[x,y,z,w] 3-6
    // fx 7
    // fy 8
    // cx 9
    // cy 10
    // scale 11
};
int sjsvt_send_frame(int socket , sjsvt_Frame *frame){
    static std::vector<unsigned char> tmp;
    tmp.resize(320*240*4+128);
    double * d_data = (double*)&tmp[0];
    for(int i=0;i<16;++i)d_data[i]=frame->extra_data[i];
    int extra_data_sz = 16*8;
    
    int depth_size = sjsvt_compress_depth(frame->depth, (unsigned short*)(&tmp[extra_data_sz]), 320*240)*2;
    int col_size = sjsvt_compress_color(frame->color, ((unsigned char*)&tmp[0])+depth_size+extra_data_sz,320*240*4-depth_size,320,240);
    socket=sjsn_send_data_chunked(socket,&tmp[0],depth_size+col_size+extra_data_sz);
    if(socket==-1){
        printf("Failed to Send\n");
    }
    return socket;
    
    
    
}
struct sjsvt_VideoClient{
    std::vector<sjsvt_Frame> frames;
    volatile int last_frame;
    bool should_run;
    std::string port;
    unsigned int service_id;
    unsigned int function_id;
    pthread_t thread;
    bool valid;
    unsigned int recieved_frame;
    unsigned int processed_frame;
    
    sjsvt_VideoClient(){
        frames.resize(16);
        valid=false;
        for(int i=0;i<16;++i){
            frames[i].depth = new unsigned short[320*240];
            frames[i].color = new unsigned char[320*240*3];
            
        }
        recieved_frame = processed_frame = 0;
    }
    static void* handle_video_loop(void* user_data){
        sjsvt_VideoClient * client=(sjsvt_VideoClient*)user_data;
        client->last_frame=0;
        int peer_listen_socket= -1;
        int socket = -1;
        std::vector<unsigned char> data;
        data.resize(320*240*4);
        int numbytes=0;
        while(client->should_run){
            if(peer_listen_socket==-1){
                peer_listen_socket=sjsn_bind_peer_listener(client->port.c_str());
            }
            if(socket==-1){
                usleep(100000);
                socket = sjsn_connect_peer(peer_listen_socket,client->service_id,client->function_id);
                client->recieved_frame = client->processed_frame = 0;
            }
            if ((socket = sjsn_recv_data_chunked(socket, &data[0], data.size(),&numbytes)) == -1) {
                perror("recv");
                close(socket);
                client->valid=false;
            }else{
                printf("client: received '%d'\n",numbytes);
                int next_f = (client->last_frame+1)%16;
                
                sjsvt_Frame* f = &client->frames[0]+next_f;
                double * ex_data = (double*)&data[0];
                for(int i=0;i<16;++i)f->extra_data[i]=ex_data[i];
                int extra_data_sz = 16*8;
                
                int color_start = sjsvt_decompress_depth((unsigned short*)&data[extra_data_sz],f->depth,numbytes/2,320*240)*2;
                int color_size = sjsvt_decompress_color(&data[color_start+extra_data_sz],f->color,numbytes-color_start-extra_data_sz,320,240);
                
                if(color_size)
                client->last_frame=next_f;
                else std::cout<<"Failed to decode color"<<std::endl;
                client->valid=true;
                client->recieved_frame=client->recieved_frame+1;
                std::cout<<"color_size: "<<color_start<<" "<<color_size<<std::endl;
            }
        }
        return NULL;
    }
    bool has_new_frame(){
        if(processed_frame!=recieved_frame&&valid){
            processed_frame=recieved_frame;
            return true;
        }
        return false;
    }
    void start(std::string port_str,int serv_id, int func_id){
        port = port_str;
        should_run=true;
        last_frame=0;
        this->service_id = serv_id;
        this->function_id = func_id;
        int iret1 = pthread_create( &thread, NULL, handle_video_loop, (void*) this);
        if(iret1)
        {
            fprintf(stderr,"Error - pthread_create() return code: %d\n",iret1);
            exit(EXIT_FAILURE);
        }
        
    }
    sjsvt_Frame* get_last_frame(){return &frames[0]+last_frame;}
    
    unsigned short* get_last_depth(){return frames[last_frame].depth;}
    unsigned char* get_last_color(){return frames[last_frame].color;}
    
    ~sjsvt_VideoClient(){
        should_run=false;
        
        pthread_join( thread, NULL);
        for(int i=0;i<16;++i){
            delete [] frames[i].color;
            delete [] frames[i].depth;
            
        }
    }
};
#endif

