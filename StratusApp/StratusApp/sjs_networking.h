/**
 *
 * Network Server
 *
 * Copyright (c) 2014 Skyler Saleh
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
**/
#include <iostream>
#include <unistd.h>

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdint.h>
#include <sstream>
#include <math.h>
#include <opencv2/opencv.hpp>


void sigchld_handler(int s)
{
    while(waitpid(-1, NULL, WNOHANG) > 0);
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
int bind_server(const char* port)
{
    int sockfd;  // listen on sock_fd
    struct addrinfo hints, *servinfo, *p;
    int yes=1;
    int rv;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE; // use my IP

    if ((rv = getaddrinfo(NULL, port, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return -1;
    }


    // loop through all the results and bind to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                p->ai_protocol)) == -1) {
            perror("server: socket");
            continue;
        }

        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
                sizeof(int)) == -1) {
            perror("setsockopt");
            return -1;
        }

        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("server: bind");
            continue;
        }

        break;
    }

    if (p == NULL)  {
        fprintf(stderr, "server: failed to bind\n");
        return -1;
    }

    freeaddrinfo(servinfo); // all done with this structure
    return sockfd;
}

int accept_connection(int listen_fd){
    struct sockaddr_storage their_addr; // connector's address information
    char s[INET6_ADDRSTRLEN];
    socklen_t sin_size;
    sin_size = sizeof their_addr;
    int new_fd = accept(listen_fd, (struct sockaddr *)&their_addr, &sin_size);
    if (new_fd == -1) {
        perror("accept");
        return -1;
    }
  

    inet_ntop(their_addr.ss_family,
        get_in_addr((struct sockaddr *)&their_addr),
        s, sizeof s);
    printf("server: got connection from %s\n", s);
    return new_fd;
}
int connect_socket(const char* addr, const char* port){
    int sockfd, numbytes;
    struct addrinfo hints, *servinfo, *p;
    int rv;
    char s[INET6_ADDRSTRLEN];
    
    
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    
    if ((rv = getaddrinfo(addr, port, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return -1;
    }
    
    // loop through all the results and connect to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                             p->ai_protocol)) == -1) {
            perror("client: socket");
            continue;
        }
        
        if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("client: connect");
            continue;
        }
        
        break;
    }
    
    if (p == NULL) {
        fprintf(stderr, "client: failed to connect\n");
        return -1;
    }
    
    inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
              s, sizeof s);
    printf("client: connecting to %s\n", s);
    
    freeaddrinfo(servinfo); // all done with this structure
    return sockfd;
}
int send_data(int socket, unsigned char* data, int size){
    int byte_off = 0;
    while(byte_off<size){
        int s_res=send(socket,&data[byte_off],size-byte_off,0);
        if(s_res < 0){
            return s_res;
        }
        byte_off+=s_res;
        
    }
    return 0;
}
int recv_data(int socket, unsigned char* data, int size){
    int byte_off = 0;
    while(byte_off<size){
        int s_res=recv(socket,&data[byte_off],size-byte_off,0);
        if(s_res < 0){
            return s_res;
        }
        byte_off+=s_res;
        
    }
    return byte_off;
}
int send_data_chunked(int socket, unsigned char* data, int size){
    uint32_t s =size;
    send_data(socket,(unsigned char*)&s,4);
    return send_data(socket,data,size);
}
int recv_data_chunked(int socket, unsigned char* data, int max_size){
    uint32_t s =0;
    int sz=recv_data(socket,(unsigned char*)&s,4);
    if(sz<0)return sz;
    if(s>max_size)s=max_size;
    return recv_data(socket,data,s);
}
#define REMAP_DEPTH(A) A
int compress_depth(unsigned short * in_data, unsigned short* out_data, int in_size){
    int current_count=0;
    int out_size =-1;
    int conv_current=-1;
    for(int i=0;i<in_size;++i){
        
        int d =in_data[REMAP_DEPTH(i)];
        int conv = 0;
        if(d)conv=round(logf(d)*180.);
        
        if(conv_current!=conv || current_count==31){
            if(out_size>-1){
                out_data[out_size]= conv_current|(current_count<<11);
            }
            ++out_size;
            conv_current = conv;
            current_count=0;
        }
        ++current_count;
        
    }
    if(current_count)
    out_data[out_size++]= conv_current|(current_count<<11u);

    return out_size;
}
int decompress_depth(unsigned short * in_data, unsigned short* out_data, int in_size, int max_out){
    int current_count=0;
    int current_conv=0;
    int current_out = 0;
    int current_in =0;
    while(current_out<max_out){
        if(current_count==0){
            
            int read = in_data[current_in];
            int data = read&0x7ff;
            current_count=read>>11;
            float d = data;
            
            current_conv=exp(d/180.);
            ++current_in;
        }
        out_data[REMAP_DEPTH(current_out)]=current_conv;
        current_out++;
        current_count--;
    }
   
    return current_in;
}
int compress_color(unsigned char* in_data, unsigned char* out_data, int max_out, int w, int h){
    cv::Mat col(h,w,CV_8UC3,in_data,0);
    std::vector<unsigned char> data;
    std::vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
    compression_params.push_back(80);
    cv::imencode(".jpg",col,data,compression_params);
    for(int i=0;i<data.size();++i){
        out_data[i]=data[i];
    }
    return data.size();
    
    
}
int decompress_color(unsigned char* in_data, unsigned char* out_data, int in_size, int w, int h){
    cv::Mat dat(in_size,1,CV_8UC1,in_data,0);
    cv::Mat col = cv::imdecode(dat,CV_LOAD_IMAGE_COLOR);
    unsigned char* data = col.data;
    int num_pix = col.cols*col.rows;
    for(int i=0;i<num_pix*3;++i){
        out_data[i]=data[i];
    }
    return num_pix*3;
    
    
}
struct Frame{
    unsigned char *color;
    unsigned short *depth;
    //Measured Quantities
    double extra_data[16];
    // Pos[x,y,z] 0-2
    // rot[x,y,z,w] 3-6
    // fx 7
    // fy 8
    // cx 9
    // cy 10
    // scale 11
};

void send_frame(int socket , Frame&frame){
    std::vector<unsigned char> tmp;
    tmp.resize(320*240*4+128);
    double * d_data = (double*)&tmp[0];
    for(int i=0;i<16;++i)d_data[i]=frame.extra_data[i];
    int extra_data_sz = 16*8;
    
    int depth_size = compress_depth(frame.depth, (unsigned short*)(&tmp[extra_data_sz]), 320*240)*2;
    int col_size = compress_color(frame.color, ((unsigned char*)&tmp[0])+depth_size+extra_data_sz,320*240*4-depth_size,320,240);
    
    if(send_data_chunked(socket,&tmp[0],depth_size+col_size+extra_data_sz)==-1){
        printf("Failed to Send\n");
        close(socket);
        socket=-1;
    }
}

struct VideoClient{
    std::vector<Frame> frames;
    volatile int last_frame;
    bool should_run;
    std::string host;
    std::string port;
    pthread_t thread;
    bool valid;
    VideoClient(){
        frames.resize(16);
        valid=false;
        for(int i=0;i<16;++i){
            frames[i].depth = new unsigned short[320*240];
            frames[i].color = new unsigned char[320*240*3];
            
        }
    }
    static void* handle_video_loop(void* user_data){
        VideoClient * client=(VideoClient*)user_data;
        client->last_frame=0;
        int socket = -1;
        std::vector<unsigned char> data;
        data.resize(320*240*4);
        int numbytes=0;
        while(client->should_run){
            if(socket==-1){
                usleep(100000);
                std::cout<<"Connecting to "<<client->host<<":"<<client->port<<std::endl;
                socket = connect_socket(client->host.c_str(),client->port.c_str());
            }
            if ((numbytes = recv_data_chunked(socket, &data[0], data.size())) == -1) {
                perror("recv");
                close(socket);
                socket=-1;
                client->valid=false;
            }else{
                printf("client: received '%d'\n",numbytes);
                int next_f = (client->last_frame+1)%16;
                
                Frame* f = &client->frames[0]+next_f;
                double * ex_data = (double*)&data[0];
                for(int i=0;i<16;++i)f->extra_data[i]=ex_data[i];
                int extra_data_sz = 16*8;

                int color_start = decompress_depth((unsigned short*)&data[extra_data_sz],f->depth,numbytes/2,320*240)*2;
                int color_size = decompress_color(&data[color_start+extra_data_sz],f->color,numbytes-color_start-extra_data_sz,320,240);

                if(color_size)
                client->last_frame=next_f;
                else std::cout<<"Failed to decode color"<<std::endl;
                client->valid=true;
                std::cout<<"color_size: "<<color_start<<" "<<color_size<<std::endl;
            }
        }
        return NULL;
    }
    void start(std::string host_str,std::string port_str){
        
        host = host_str;
        port = port_str;
        should_run=true;
        last_frame=0;
        int iret1 = pthread_create( &thread, NULL, handle_video_loop, (void*) this);
        if(iret1)
        {
            fprintf(stderr,"Error - pthread_create() return code: %d\n",iret1);
            exit(EXIT_FAILURE);
        }
        
    }
    Frame* get_last_frame(){return &frames[0]+last_frame;}

    unsigned short* get_last_depth(){return frames[last_frame].depth;}
    unsigned char* get_last_color(){return frames[last_frame].color;}
    
    ~VideoClient(){
        should_run=false;
        
        pthread_join( thread, NULL);
        for(int i=0;i<16;++i){
            delete [] frames[i].color;
            delete [] frames[i].depth;

        }
    }
};

