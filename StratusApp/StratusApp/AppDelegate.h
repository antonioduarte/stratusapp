//
//  AppDelegate.h
//  StratusApp
//
//  Created by Antonio Duarte on 12/20/14.
//  Copyright (c) 2014 StratusTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

