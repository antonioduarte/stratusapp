//
//  ViewController.h
//  StratusApp
//
//  Created by Antonio Duarte on 12/20/14.
//  Copyright (c) 2014 StratusTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#define HAS_LIBCXX
#import <Structure/Structure.h>

@interface ViewController : UIViewController <STSensorControllerDelegate>

@end