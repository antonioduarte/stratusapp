//
//  main.m
//  StratusApp
//
//  Created by Antonio Duarte on 12/20/14.
//  Copyright (c) 2014 StratusTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
